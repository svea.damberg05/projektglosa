<?php
class Words extends Controller
{
    public function add()
    {
        try {
            (new Word)->add($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400); 
        }
        $this->json([], 201); // 201 för lyckat tillägg
    }

    public function get()
    {
        try {
            $words = (new Word)->get($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
        $this->json(compact('words'));

    }

    public function get_by_language()
    {
        try {
            $words = (new Word)->get_by_language($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
        $this->json(compact('words'));

    }

    public function remove()
    {
        try {
            (new Word)->remove($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }
        $this->json();

    }

    public function answer()
    {
        try {
            $answer = (new Word)->answer($_POST);
            
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }
        $this->json(compact('answer'));

    }

    public function edit_word(){
        try {
            (new Word)->edit_word($_POST);
            
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }
        $this->json();

    }

    public function edit_translation(){
        try {
            (new Word)->edit_translation($_POST);
            
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }
        $this->json();

    }

}