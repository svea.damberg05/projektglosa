<?php

class Users extends Controller
{
    public function register()
    {
        try {
            $token = (new User)->register($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }

        $this->json(compact('token'), 201); 
    }

    public function login()
    {
        try {
            $token = (new User)->authenticate($_POST);
        } catch (Exception $e) {
            $this->error($e->getMessage(), 400);
        }

        $this->json(compact('token'));
    }

    public function logout()
    {
        try {
            $user = (new User)->logged_in($_POST['token']);
            (new User)->logout($user['id']);
        } catch (Exception $e) {
            $this->error($e->getMessage(), 401);
        }
        $this->json();
    }

}