<?php

class Word extends Model
{
    public $table = 'words';

    // Lägger till en ny glosa i databasen
    public function add(array $data)
    {
        extract($data);

        $user_id = (new User)->get_id($token);

        if (empty($word) || empty($translation) || empty($language) || empty($token)) {
            throw new Exception("Alla fält måste fyllas i");
        }

        $this->query("INSERT INTO $this->table (user_id, word, translation, language) VALUES (:user_id, :word, :translation, :language)", [
            'user_id' => $user_id,
            'word' => $word,
            'translation' => $translation,
            'language' => $language
        ]);

    }

    // Hämtar alla glosor
    public function get(array $data)
    {
        extract($data);

        $user = (new User)->logged_in($token);
 //
        $words = $this->query("SELECT * FROM $this->table WHERE user_id = :user_id", [
            'user_id' => $user['id']
        ])->fetchAll();

        if (!$words) {
            throw new Exception("Finns inga ord");
        }

        return $words ?: null;
    }

    // Hämtar alla glosor från ett visst språk
    public function get_by_language(array $data)
    {
        extract($data);

        $user = (new User)->logged_in($token);

        $words = $this->query("SELECT * FROM $this->table WHERE user_id = :user_id AND language = :language", [
            'user_id' => $user['id'],
            'language' => $language
        ])->fetchAll();

        if (!$words) {
            throw new Exception("Finns inga ord");
        }

        return $words ?: null;

    }

    // Raderar en glosa
    public function remove(array $data)
    {
        extract($data);
        (new User)->logged_in($token);

        $word_exists = $this->query("SELECT * FROM $this->table WHERE id = :id", [
            'id' => $id
        ])->fetch();
        if (!$word_exists) {
            throw new Exception("Ordet finns inte");
        }

        $this->query("DELETE FROM $this->table WHERE id = :id", [
            'id' => trim($id)
        ]);

    }

    // Jämför ett svar från databasem med ett svar från användaren
    public function answer(array $data)
    {
        extract($data);

        (new User)->logged_in($token);

        // Hämtar rätt svar
        $correct_answer = $this->query("SELECT * FROM $this->table WHERE id = :id", [
            'id' => $id
        ])->fetch();

        return ($correct_answer['translation'] == $answer);

    }

    // Uppdaterar en glosa 
    public function edit_word(array $data) {
        extract($data);

        (new User)->logged_in($token);

        // Kontollerar att ordet finns
        $word_exists = $this->query("SELECT * FROM $this->table WHERE id = :id", [
            'id' => $id
        ])->fetch();
        if (!$word_exists) {
            throw new Exception("Ordet finns inte");
        }

        $this->query("UPDATE $this->table SET word = :word WHERE id = :id", [
            'id' => $id,
            'word' => $word
        ]);
    }

    // Uppdaterar en översättning till en glosa
    public function edit_translation(array $data) {
        extract($data);

        (new User)->logged_in($token);

        // Kontollerar att ordet finns
        $word_exists = $this->query("SELECT * FROM $this->table WHERE id = :id", [
            'id' => $id
        ])->fetch();
        if (!$word_exists) {
            throw new Exception("Ordet finns inte");
        }

        $this->query("UPDATE $this->table SET translation = :translation WHERE id = :id", [
            'id' => $id,
            'translation' => $translation
        ]);
    }


}