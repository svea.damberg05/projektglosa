<?php

class User extends Model
{
    public $table = 'users';

    // Generear en token till användaren och lägger in den i databasen
    public function generate_token($user_id)
    {
    $token = bin2hex(random_bytes(32));

        $this->query("UPDATE $this->table SET token = :token WHERE id = :id", [
            'id' => $user_id,
            'token' => $token
        ]);

        return $token;
    }

    // Returnerar användarens id
    public function get_id($token) {
        $user = $this->query("SELECT id FROM $this->table WHERE token = :token", [
            'token' => $token
        ])->fetch();

        if (!$user) {
            throw new Exception("Kunde inte identifiera anvädnaren.");
        }

        return $user['id'] ?: null;
    }

    // Kontrollerar om användaren är inloggad
    public function logged_in($token)
    {
        $user = $this->query("SELECT * FROM $this->table WHERE token = :token", [
            'token' => $token
        ])->fetch();
        if (!$user) {
            throw new Exception("Ogiltig token");
        }

        return $user ?: null;
    }

    // Loggar in användaren
    public function authenticate(array $data)
    {
        extract($data);

        $user = $this->query("SELECT * FROM $this->table WHERE username = :username", [
            'username' => $username
        ])->fetch();

        if (!$user) {
            throw new Exception("Det finns ingen användare med detta användarnamn");
        }

        if (password_verify($password, $user['password'])) {
            return $this->generate_token($user['id']);
        } else {
            throw new Exception("Lösenordet stämde inte");
        }
    }

    // Registrerar en ny användare
    public function register(array $data)
    {
        extract($data);

        if (empty($username) || empty($password) || empty($password_confirm)) {
            throw new Exception("Alla fält måste fyllas i");
        }

        $existingUser = $this->query("SELECT * FROM $this->table WHERE username = :username", [
            'username' => $username
        ])->fetch();

        if ($existingUser) {
            throw new Exception("Det finns redan en användare med det här användarnamnet");
        }

        if ($password !== $password_confirm) {
            throw new Exception("Lösenorden stämde inte överens");
        }

        $hashed_password = password_hash($password, PASSWORD_BCRYPT);
        $token = bin2hex(time() . random_bytes(16));


        $user_id = $this->query("INSERT INTO $this->table (username, password, token) VALUES (:username, :password, :token)", [
            'username' => $username,
            'password' => $hashed_password,
            'token' => $token
        ]);

        return $token;
    }

    // Loggar ut användaren genom att generara en ny token
    public function logout($user_id)
    {
        $this->generate_token($user_id);
    }
}