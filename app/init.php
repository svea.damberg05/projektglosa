<?php

/**
 * Registers an autoloader function to automatically include class files on-demand.
 * 
 * This autoloader iterates through all PHP files within the 'app' directory and its subdirectories,
 * requiring the file that matches the class name being instantiated. This eliminates the need for
 * manual inclusion of class files, streamlining the class usage process.
 */
spl_autoload_register(function ($class) {
    // Search for all PHP files within the 'app' directory and its subdirectories
    $files = glob('app/*/*.php', GLOB_NOSORT);

    // Iterate through each file to find a match for the requested class
    foreach ($files as $file) {
        // Check if the current file name matches the requested class name
        if (strpos($file, $class . '.php') !== false) {
            require_once $file; // Include the class file
            break; // Exit the loop once the class file is included
        }
    }
});
