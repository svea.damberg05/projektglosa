<?php

/**
 * The central entry point of the application responsible for handling requests.
 * This class parses the URL, identifies the appropriate controller and method to handle the request,
 * and passes any additional URL segments as parameters to the controller method.
 */
class App
{
    /**
     * The default controller if none is specified in the URL.
     *
     * @var string
     */
    protected $controller = 'home';

    /**
     * The default method to be called if none is specified in the URL.
     *
     * @var string
     */
    protected $method = 'index';

    /**
     * Parameters to be passed to the method, extracted from the URL.
     *
     * @var array
     */
    protected $params = [];

    /**
     * Constructor parses the URL, sets the controller, method, and parameters.
     * It then calls the specified controller and method with the parameters.
     */
    public function __construct()
    {
        // Parse the URL into a segment array
        $url = $this->parse_url();

        // Set the controller if specified in the URL and exists
        if (isset($url[0]) && is_subclass_of($url[0], 'Controller')) {
            $this->controller = array_shift($url);
        }

        // Instantiate the controller class
        $this->controller = new $this->controller;

        // Set the method if specified in the URL and exists in the controller
        if (isset($url[0]) && method_exists($this->controller, $url[0])) {
            $this->method = array_shift($url);
        }

        // Remaining URL segments are treated as parameters
        $this->params = $url;

        // Call the controller method with parameters
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    /**
     * Parses the URL into an array of segments.
     * 
     * @return array The URL segments as an array.
     */
    protected function parse_url()
    {
        if (isset($_GET['url'])) {
            return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }

        return [];
    }
}
