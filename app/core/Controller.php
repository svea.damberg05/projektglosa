<?php
class Controller
{
    public function json(array $data = [], $code = 200)
    {
        header('Content-Type: application/json');

        $data['status_code'] = $code;

        echo json_encode($data);

        exit;
    }

    protected function error($error = 'An error occured', $code = 404) {
        $this->json(compact('error'), $code);
    }
}
