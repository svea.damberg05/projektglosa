<?php


class Model
{
    /**
     * Database connection object.
     *
     * @var PDO
     */
    protected $db;

    /**
     * The name of the table associated with the model.
     *
     * @var string
     */
    public $table;

    /**
     * Constructor to establish a database connection using PDO.
     * Connection parameters are fetched from the application's configuration file.
     *
     * @throws PDOException If connection to the database fails.
     */
    public function __construct()
    {
        $config = require('app/config.php');

        $dsn = "mysql:host={$config['database']['host']};port={$config['database']['port']};dbname={$config['database']['dbname']};charset=utf8mb4";

        try {
            $this->db = new PDO($dsn, $config['database']['user'], $config['database']['password'], [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    /**
     * Executes a SQL query using prepared statements.
     * Automatically determines the type of query (SELECT, INSERT, UPDATE, DELETE)
     * and returns appropriate results: last insert ID for INSERT, row count for UPDATE/DELETE,
     * and PDOStatement for SELECT.
     *
     * @param string $sql The SQL query to execute.
     * @param array $params Parameters to bind to the SQL query.
     * @return mixed The result of the query execution.
     */
    public function query(string $sql, array $params = [])
    {
        $stmt = $this->db->prepare($sql);

        foreach ($params as $key => $value) {
            $stmt->bindValue(":$key", $value, PDO::PARAM_STR);
        }

        $stmt->execute();

        if (preg_match('/^INSERT/i', $sql)) {
            return $this->db->lastInsertId();
        } elseif (preg_match('/^(UPDATE|DELETE)/i', $sql)) {
            return $stmt->rowCount();
        } else {
            return $stmt;
        }
    }

    /**
     * Retrieves a single record from the database by its ID.
     *
     * @param int $id The ID of the record to find.
     * @return array The fetched record.
     */
    public function find(int $id)
    {
        return $this->query("SELECT * FROM $this->table WHERE id = :id", ['id' => $id])->fetch();
    }

    /**
     * Retrieves all records from the associated table.
     *
     * @return array An array of all records in the table.
     */
    public function all()
    {
        return $this->query("SELECT * FROM $this->table")->fetchAll();
    }
}
