<?php

/**
 * Entry point of the PHP web application.
 *
 * Initiates the user session to track user state across requests, includes the main application
 * initialization file, and creates an instance of the App class to handle the request.
 */

// Include the main application initialization file that sets up the application environment,
// autoloads classes, and performs any necessary preliminary configurations.
require_once 'app/init.php';

// Instantiate the App class which serves as the central point for handling the incoming request,
// routing it to the appropriate controller and method based on the request URL.
$app = new App();
