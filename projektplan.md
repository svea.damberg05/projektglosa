# Projektplan

## Bakgrund
Detta projekt går ut på att skapa ett glosprogram som låter en användare lägga till och svara på  glosor för att kunna lära sig nya ord. Projektet är ett samarbete mellan Webbserverprogrammering 1 och Programmering 2. Detta innebär att glosprogrammet kommer vara uppbyggt av en Java-del i Programmering 2 där use-cases och GUI skapas samt en API-del i Webbserverprogrammering 1 där endpoints skapas.

Projektet kommer att använda sig av en MVC-struktur. Det vill säga att applikationen är uppdelad i tre delar: model, view och controller. All logik och hämtning, lagring och bearbetning av data sker i model medan delen som användaren interagerar med är view. View tillåter datan från model att presenteras på ett tilltalande sätt och kommer i detta projekt vara ett GUI i Java. Controller agerar som ett gränssnitt mellan de två komponenterna vilket innebär att model och view aldrig interagerar. Controller kan alltså ses som mellanhanden i strukturen som kommunicerar med vardera komponent och skiljer dem åt. På detta sätt får man en organiserad struktur som separerar datalogik och presentationslogik vilket gör applikationen flexibel.

## Projektupplägg
### Mål
Målet med detta projekt är att skapa ett fullt fungerande glosprogram som tillåter en användare att logga in, logga ut, lägga till glosor, ta bort glosor och svara på en glosa. Ytterligare funktioner kan även tillkomma under projektets gång. Detta ska göras innom en begränsad tidsram och resultera i en fullt funktionerande applikation. 
### Kravspecifikation
- GUI och logik utvecklat i Java där modell och grafiskt gränssnitt ska separeras enligt MVC-principen.
- Data sparas i MySQL via ett REST-API gjort i PHP.
- Endpoints med:
    - Unika användare med inloggning och utloggning
    - Kunna skapa nya glosor.
    - Kunna radera glosa.
    - Kunna hämta glosa.
    - Kunna "svara på" en glosa och få veta om det var rätt. Rättning ska ske via end-point.

### Verktyg
De verktyg som kommer att användas är mjukvaran Eclipse där programmeringsspråket Java används. Jag kommer även använda mjukvaran Visual Studio Code för att skapa mitt API i programmeringsspråket PHP. Jag kommer att använda GUI:et phpMyAdmin för att hantera databasen samt webbläsartillägget Talend API Tester för att testa mitt API.


### Kommunikation med kunden
För att kommunicera applikationen med kunden är det möjligt att lägga till glosprogrammet som ett tillägg på skrivbordet. Detta ger snabb och enkel tillgång till programmet för användaren.

### Dokumentation
Dokumentation av arbetsgången kommer ske i form av en tidsplan som jag uppdaterar efter varje vecka. På så sätt fördelas tiden efter uppgifter och jag kan hålla arbetet strukturerat.


## Teknisk specifikation
### Java-delen
#### Use-cases 
I mitt program ska följande use-cases finnas:
- Logga in

- Logga ut

- Skapa konto

- Skapa en glosa

- Svara på en glosa

- Hämta glosor

- Hämta glosor efter språk

- Radera en glosa

- Redigera en glosa

- Redigera svaret till en glosa

#### GUI
##### Startvision:
![GUI1](ProjektGlosa.drawio.png)

##### Slutprodukt:

![GUI2](ProjektGlosaFinal.drawio.png)

#### UML
![UML](Glosa.drawio.png)

#### Databasdiagram
Databasen som ligger på student.oedu använder sig av följande tabeller.
| Users |
| -----|
| id |
| username |
| password |
| token |

| Words | 
|------|
| id |
| user_id |
| word |
| translation |
| language |
---
### API-delen
### End-points
Dessa end-points kommer mitt projekt att innehålla:
#### Registrera
 - URL `/users/register` : Registrera en ny användare i utbyte mot en token som krävs i andra endpoints
- Skicka 
    - username
    - password
    - password_confirm
- Svar
    - token
    - statuscode
#### Logga in
- URL `/users/login` : Skickar användarnamn och lösenord i utbyte mot en token som krävs i de andra endpoints
- Skicka 
    - username
    - password
- Svar
    - token
    - statuscode
#### Logga ut
- URL `/users/logout` : Genererar en ny token åt användaren
- Skicka
    - token 
- Svar
    - statuscode
#### Skapa glosa
- URL `/words/add` : Skickar token, ord, översättning samt språk och lägger in i databasen
- Skicka
    - token
    - glosa
    - svar
    - språk
- Svar
    - statuscode

#### Hämta en glosa
- URL `words/get` : Skickar en token i utbyte mot en gloslista
- Skicka   
    - token
- Svar
    - array med ord
    - statuscode

#### Hämta en glosa efter språk
- URL `words/get_by_language` : Skickar en token i utbyte mot en gloslista sorterad efter språk
- Skicka   
    - token
    - språk
- Svar
    - array med ord
    - statuscode
#### Rätta svar
- URL `/words/answer` : Skickar ett svar och jämför med översättningen i databasen
- Skicka 
    - token
    - ordets id
    - svar
- Svar
    - true/false 
    - statuscode
#### Radera glosa
- URL `/words/remove` : Skickar id:et av ett ord och raderar det från databasen
- Skicka
    - token
    - ordets id
- Svar
    - statuscode 
 
 #### Redigera glosa
- URL `words/edit_word` : Skickar id:et av ett ord samt ett nytt ord och uppdaterar databasen under word
- Skicka 
    - token
    - ordets id
    - nytt ord
- Svar
    - statuscode

 #### Redigera översättning
- URL `words/edit_translation` : Skickar id:et av en översättning samt en ny översättning och uppdaterar databasen under translation
- Skicka 
    - token
    - ordets id
    - ny översättning
- Svar
    - statuscode

## Tidsplan
![Tidsplan](ProjektplanGlosa.PNG) 

## Tester
Tester av projektet utfördes genomgående under projektet på olika sätt. Dels använde jag mig av Talend API-tester för att kontrollera JSON-strängarna jag fick från model. Genom att ge alla metoder i model korrekta felkoder kunde jag enkelt avläsa om datahämtningen var korrekt eller inte och det förtydligade även vad som skett på ett precist sätt. Till exempel användes felkod 201 för endpoints register och add, denna kod innebär att frågan lades till i databasen. Likaså användes felkoden 401 i situationer där användarens token inte var giltig.

Då det kom till Java-delen testade jag dess funktion egentligen konstant då jag skapade användargränssnittet. Genom att starta applikationen kunde jag kontrollera så att min design blev som önskad och jämförde även denna med min startvision för GUI:et
## Diskussion
### Utvärdering och reslutat
Utseendemässigt skiljer sig den slutgiltiga produkten från startvisionen av GUI:et till viss del. Anledningen till att dessa förändringar gjordes var framförallt på grund av att jag från början ville skapa en baslinje att arbeta ifrån då det kom till GUI:ets design. När jag sedan buggde produkten visade det sig att en del planer inte fungerade eller inte var möjliga att genomföra, till exempel fanns inte tiden till för att skapa en ordentlig hemsida så därför använde jag en alternativ lösning med endast tre knappar istället för en "användarsida" som jag från början velat göra. Ytterligare använde jag mig inte av någon statistik i glosprojektet eftersom att tiden inte fanns till.

Slutprodukten lever upp till de krav som sattes på projektet. Den tillåter en användare att lägga till glosor, ta bort glosor, logga in och ut samt svara på glosor. Dessutom har kravspecifiaktionen uppnåts där applikationen är byggd enligt MVC-principen, all data sparas via ett API på en databas och samtliga endpoints finns och fungerar. Dessutom tillåter den användaren att lära sig glosor på ett effektivt och användarvänligt sätt. 

Arbetet sträckte sig från vecka 13 till vecka 22 där kontinuerliga uppdateringar gjordes i tidplanen eftersom arbetet fortskred. Jag stötte inte på några större problem, och de jag gjorde gick relativt snabbt att lösa med hjälp från handledare eller internet. Anledningen till att jag inte hann göra alla funktioner jag från början tänkt berodde huvudsakligen på att jag var en aning tidoptimistisk i det tidiga skedet. Om Jag haft mer tid hade jag först och främst fokuserat på att göra en ändring i användargränssnittet som tillåter en användare att svara på flera glosor åt gången i stället för en åt gången. Det tror jag gjort upplevelsen mer lärorik och projektet hade på så sätt levt upp till sina syften och mål ytterligare. Jag skulle även vilja göra det möjligt att byta plats på glosan på valt språk och svaret på svenska så att man kan lära sig åt båda hållen. 